# Samba plugins

**Provides Ansible playbooks for a Samba-server and a Samba-client configuration on SURF Research Cloud.**

## Samba

Samba is a tried-and-true technology to share storage between hosts.

This page in our user documentation gives a short explanation and steps to configure Samba manually:
https://servicedesk.surf.nl/wiki/x/d-fTAQ

The components offer an automatic way to setup shared storage with Samba.

They can be added to other SURF Research Cloud catalog items when shared storage is needed.

The server shares the directory /data/volume_2/samba-share.

The clients mount the shared storage at /samba_share. (Default, can be adjusted in *samba-client-folder* parameter)

These plugins are also available on SURF Research Cloud by themselves, "wrapped" in the catalog items "Samba Server" and "Samba Client (Linux)".

### parameters:

* samba-client-folder
* samba-share
* smbuser
* samba_password

## Requirements

- The OS of the server is expected to be of a Debian-family distro (probably Ubuntu)
- The OS of the client is expected to be of a Debian-family distro (probably Ubuntu). Unless somebody volunteers to implement the Windows-client version and one for the RedHat family, too.
- The server should have an external volume attached and mounted to /data/volume_2 (this is the default for external volumes)
- The smbuser password must be available as a collaboration secret with the key "samba_password"
- The catalog item of the client must require an interactive parameter that receives the server's private ip-address as user-input.
- Both server and clients should be connected to the same private network


